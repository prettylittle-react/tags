import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Tags.scss';

/**
 * Tags
 * @description [Description]
 * @example
  <div id="Tags"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Tags, {
    	title : 'Example Tags'
    }), document.getElementById("Tags"));
  </script>
 */
class Tags extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'tags';
	}

	render() {
		const {items} = this.props;

		// TODO: allow for links
		const tags = items
			? items.map((item, i) => {
					return (
						<li key={`tag-${i}`}>
							<Text content={item} />
						</li>
					);
			  })
			: null;

		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<ul>{tags}</ul>
			</div>
		);
	}
}

Tags.defaultProps = {
	items: null,
	children: null
};

Tags.propTypes = {
	items: PropTypes.array,
	children: PropTypes.node
};

export default Tags;
